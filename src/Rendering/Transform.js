import { mat4, quat, vec3 } from '../glMatrix/gl-matrix.js';

const pi = 3.14159265359;
export function deg2rad(degree) {
    return (degree / 180.0) * pi;
}

export function rad2Deg(radians) {
    return (degree * 180) / pi;
}

export class Transform {
    constructor(position, rotation, scale) {
        this.position = position;
        this.scale = scale;
        this.rotation = quat.fromEuler(quat.create(), rotation[0], rotation[1], rotation[2]);
        this.objectToWorld = mat4.create();
        this.worldToObject = mat4.create();
        this.calculateMatrix();
    }

    rotate(x, y, z) {
        quat.rotateX(this.rotation, this.rotation, deg2rad(x));
        quat.rotateY(this.rotation, this.rotation, deg2rad(y));
        quat.rotateZ(this.rotation, this.rotation, deg2rad(z));
        this.calculateMatrix();
    }

    rotateAround(x, y, z, origin) {
        const rotation = quat.fromEuler(quat.create(), x, y, z);
        const translation = vec3.subtract(vec3.create(), origin, this.position);
        this.position = origin;
        quat.rotateX(this.rotation, this.rotation, deg2rad(x));
        quat.rotateY(this.rotation, this.rotation, deg2rad(y));
        quat.rotateZ(this.rotation, this.rotation, deg2rad(z));
        vec3.transformQuat(translation, translation, rotation);
        vec3.subtract(this.position, this.position, translation);
        this.calculateMatrix();
    }

    calculateMatrix() {
        mat4.fromRotationTranslationScale(this.objectToWorld, this.rotation, this.position, this.scale);
        mat4.invert(this.worldToObject, this.objectToWorld);
    }
}
import { vec3, mat4 } from '../glMatrix/gl-matrix.js';
import { deg2rad } from '../Rendering/Transform.js';

export class Camera {
    static createDefault() {

        const viewMatrix = mat4.lookAt(mat4.create(),
            vec3.fromValues(0, 0, -10), vec3.create(), vec3.fromValues(0, 1, 0));

        const projectionMatrix = mat4.perspective(mat4.create(), deg2rad(65.0),  1, 0.1, 200);

        return new Camera(viewMatrix, projectionMatrix);
    }

    constructor(viewMatrix, projectionMatrix) {
        this.viewMatrix = viewMatrix;
        this.projectionMatrix = projectionMatrix;
    }
}
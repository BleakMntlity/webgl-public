import { Mesh } from '../Assets/Mesh.js';

export class Batch {
    constructor(meshes) {
        this.meshes = {};

        let vertexOffset = 0;
        let indexOffset = 0;
        let vertices = [];
        let indices = [];
        for (let i = 0; i < meshes.length; ++i) {
            const mesh = meshes[i];
            vertices = vertices.concat(mesh.vertexData);
            indices = indices.concat(mesh.indices.map(idx => idx + vertexOffset));
            this.meshes[mesh] = {
                offset: indexOffset,
                length: mesh.indices.length
            }

            vertexOffset = mesh.vertexData.length;
            indexOffset = mesh.indices.length;
        }

        this.vertices = new Float32Array(vertices);
        this.indices = new Uint16Array(indices);
    }
}
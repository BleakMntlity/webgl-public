import { mat4, quat, vec3 } from '../glMatrix/gl-matrix.js';
const pi = 3.14159265359;
export function deg2rad(degree) {
    return (degree / 180.0) * pi;
}
export class Light {
    constructor(position) {
        this.position = position;
        this.lightMatrix = mat4.lookAt(mat4.create(), position, vec3.fromValues(0, 0, 0), vec3.fromValues(0, 1, 0));
        this.projectionMatrix = mat4.perspective(mat4.create(), deg2rad(65.0), 1, 0.1, 200);
    }

    illuminate(target) {
        mat4.lookAt(this.lightMatrix, this.position, target, vec3.fromValues(0, 1, 0));
    }
}
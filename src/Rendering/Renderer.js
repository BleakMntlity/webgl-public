import { RenderingContext } from '../RenderingContext.js';
import { vec3, mat4, quat } from '../glMatrix/gl-matrix.js';
import { ShaderLoader } from '../Assets/ShaderLoader.js';

const biasMatrix = mat4.fromValues(
    0.5, 0.0, 0.0, 0.0,
    0.0, 0.5, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.5, 0.5, 0.5, 1.0);

export class Renderer {
    constructor(mesh, batch, shader, shadowShader, texture, transform) {
        this.mesh = mesh;
        this.batch = batch;
        this.shader = shader;
        this.shadowShader = shadowShader;
        this.texture = texture;
        this.transform = transform;

        this.mvMatrix = mat4.create();
        this.mvpMatrix = mat4.create();
        this.mlMatrix = mat4.create();
        this.mlpMatrix = mat4.create();
    }

    update() {
        this.transform.rotate(Math.random(), Math.random(), Math.random());
        this.transform.rotateAround(Math.random(), Math.random(), Math.random(), vec3.create());
    }

    drawShadow(gl, light) {
        RenderingContext.ensureBatchIsBound(this.batch);
        RenderingContext.ensureShaderIsBound(this.shadowShader);

        const vertex = gl.getAttribLocation(this.shadowShader, 'vertex');
        gl.vertexAttribPointer(vertex, 3, gl.FLOAT, false, this.mesh.perVertexSize, this.mesh.attributeOffsets.vertex);
        gl.enableVertexAttribArray(vertex);

        const modelLighProjectionUniform = gl.getUniformLocation(this.shadowShader, 'mlpMatrix');

        mat4.multiply(this.mlMatrix, light.lightMatrix, this.transform.objectToWorld);
        mat4.multiply(this.mlpMatrix, light.projectionMatrix, this.mlMatrix);

        gl.uniformMatrix4fv(modelLighProjectionUniform, false, this.mlpMatrix);

        const elementsRange = this.batch.meshes[this.mesh];
        gl.drawElements(this.mesh.drawMode, elementsRange.length, gl.UNSIGNED_SHORT, elementsRange.offset);
    }

    draw(gl, light, camera) {
        RenderingContext.ensureShaderIsBound(this.shader);
        RenderingContext.ensureBatchIsBound(this.batch);
        RenderingContext.ensureTextureIsBound(this.texture);

        const vertex = gl.getAttribLocation(this.shader, 'vertex');
        const normal = gl.getAttribLocation(this.shader, 'normal')
        const texcoord = gl.getAttribLocation(this.shader, 'texcoord');

        if (vertex >= 0) {
            gl.vertexAttribPointer(vertex, 3, gl.FLOAT, false, this.mesh.perVertexSize, this.mesh.attributeOffsets.vertex);
            gl.enableVertexAttribArray(vertex);
        }
        if (normal >= 0) {
            gl.vertexAttribPointer(normal, 3, gl.FLOAT, false, this.mesh.perVertexSize, this.mesh.attributeOffsets.normal);
            gl.enableVertexAttribArray(normal);
        }
        if (texcoord >= 0) {
            gl.vertexAttribPointer(texcoord, 2, gl.FLOAT, false, this.mesh.perVertexSize, this.mesh.attributeOffsets.uvs);
            gl.enableVertexAttribArray(texcoord);
        }


        const mlpMatrixUniform = gl.getUniformLocation(this.shader, 'mlpMatrix');
        const shadowMapUniform = gl.getUniformLocation(this.shader, 'shadowMap');
        const textureUniform = gl.getUniformLocation(this.shader, 'texture');
        const mvpMatrixUniform = gl.getUniformLocation(this.shader, 'mvpMatrix');
        const worldToObject = gl.getUniformLocation(this.shader, 'worldToObject');
        const lightDirUniform = gl.getUniformLocation(this.shader, 'lightDirection');

        mat4.multiply(this.mvMatrix, camera.viewMatrix, this.transform.objectToWorld);
        mat4.multiply(this.mvpMatrix, camera.projectionMatrix, this.mvMatrix);

        mat4.multiply(this.mlMatrix, light.lightMatrix, this.transform.objectToWorld);
        mat4.multiply(this.mlpMatrix, light.projectionMatrix, this.mlMatrix);
        mat4.multiply(this.mlpMatrix, biasMatrix, this.mlpMatrix);

        gl.uniform1i(shadowMapUniform, 0); // 0th active texture (bound by context);
        gl.uniform1i(textureUniform, 1); // 1st active texture (bound by context);
        gl.uniformMatrix4fv(mvpMatrixUniform, false, this.mvpMatrix);
        gl.uniformMatrix4fv(worldToObject, false, this.transform.worldToObject);
        gl.uniformMatrix4fv(mlpMatrixUniform, false, this.mlpMatrix);

        const lightRot = mat4.getRotation(quat.create(), light.lightMatrix);
        const lightDir = vec3.transformQuat(vec3.create(), vec3.fromValues(0, 0, 1), lightRot);
        gl.uniform3fv(lightDirUniform, lightDir);

        const elementsRange = this.batch.meshes[this.mesh];
        gl.drawElements(this.mesh.drawMode, elementsRange.length, gl.UNSIGNED_SHORT, elementsRange.offset);
    }
}
import { MeshLoader } from './Assets/MeshLoader.js';
import { vec3, mat4 } from './glMatrix/gl-matrix.js';
import { Camera } from './Rendering/Camera.js';
import { Batch } from './Rendering/Batch.js';
import { Renderer } from './Rendering/Renderer.js';
import { Transform } from './Rendering/Transform.js';
import { ShaderLoader } from './Assets/ShaderLoader.js';
import { Texture } from './Assets/Texture.js';
import { Light } from './Rendering/Light.js';
import { UiProfilerView } from './UI/UiProfilerView.js';

let gl;
let currentBatch;
let currentShader;
let currentTexture;
let renderers;
let background;
let sceneCamera = Camera.createDefault();
let light = new Light(vec3.fromValues(0, 0, -8));
let texBuffer;
let shadowMap;
let shadowRenderTarget;
let uiProfilerView;

const baseUrl = window.location.hostname === 'localhost' ? 'http://localhost:8000/' : 'http://fabianhanselmann.com/WebGL/src/';
export class RenderingContext {
    static start() {
        uiProfilerView = new UiProfilerView();
        const canvas = document.querySelector('#webglCanvas');
        gl = canvas.getContext('webgl');

        if (!gl) {
            console.error('no web gl support');
            return;
        }

        shadowMap = RenderingContext.createShadowMap();
        shadowRenderTarget = RenderingContext.createShadowRenderTarget(shadowMap);

        gl.enable(gl.DEPTH_TEST);

        const vertexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);

        const indexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);

        texBuffer = gl.createTexture();
        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, texBuffer);

        renderers = RenderingContext.createRenderers();

        setInterval(RenderingContext.update, 16);
    }

    static createRenderers() {
        const mesh = MeshLoader.loadPyramid();
        const shader = ShaderLoader.loadStandardShader(gl);
        const shadowShader = ShaderLoader.loadShadowShader(gl);
        const renderers = [];
        const backgroundQuad = MeshLoader.loadQuad();
        const texture = Texture.loadFromUrl(128, 128, baseUrl + 'Assets/Content/Textures/pyramidtexture.jpeg');

        const batch = new Batch([mesh]);

        for (let i = 0; i < 100; ++i) {
            const x = (Math.random() - 0.5) * 5;
            const y = (Math.random() - 0.5) * 5;
            const z = (Math.random() - 0.5) * 5;
            const rotX = Math.random() * 360;
            const rotY = Math.random() * 360;
            const rotZ = Math.random() * 360;
            const transform = new Transform(vec3.fromValues(x, y, z), vec3.fromValues(rotX, rotY, rotZ), vec3.fromValues(1, 1, 1));
            renderers.push(new Renderer(mesh, batch, shader, shadowShader, texture, transform));
        }

        const transform = new Transform(vec3.create(), vec3.create(), vec3.fromValues(1, 1, 1));
        background = (new Renderer(backgroundQuad, new Batch([backgroundQuad]), ShaderLoader.loadBackgroundShader(gl),
            shadowShader, texture, transform));

        return renderers;
    }

    static createShadowMap() {
        const shadowMap = gl.createTexture();
        const width = gl.canvas.clientWidth;
        const height = gl.canvas.clientHeight;
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, shadowMap);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        return shadowMap;
    }

    static createShadowRenderTarget(shadowMap) {
        const renderTarget = gl.createFramebuffer();

        gl.bindFramebuffer(gl.FRAMEBUFFER, renderTarget);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, shadowMap, 0);

        gl.enable(gl.DEPTH_TEST);

        gl.bindFramebuffer(gl.FRAMEBUFFER, null);

        return renderTarget;
    }

    static update() {
        uiProfilerView.startFrame();

        for (let i in renderers) {
            renderers[i].update();
        }
        // const litPosition = renderers[0].transform.position;
        // light.illuminate(litPosition);
        RenderingContext.drawShadows();
        RenderingContext.drawRenderers();

        uiProfilerView.endFrame(renderers.length);
    }

    static drawShadows() {
        gl.bindFramebuffer(gl.FRAMEBUFFER, shadowRenderTarget);
        gl.enable(gl.DEPTH_TEST);
        gl.clearColor(1, 1, 1, 1);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        for (let i in renderers) {
            uiProfilerView.onDrawCalled();
            renderers[i].drawShadow(gl, light);
        }
    }

    static drawRenderers() {
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        gl.bindTexture(gl.TEXTURE_2D, texBuffer);

        gl.clearColor(0.2, 0.2, 0.2, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        gl.depthMask(false);
        background.draw(gl, light, sceneCamera);
        gl.depthMask(true);

        for (let i in renderers) {
            uiProfilerView.onDrawCalled();
            renderers[i].draw(gl, light, sceneCamera);
        }
    }

    static ensureShaderIsBound(shader) {
        if (currentShader !== shader) {
            uiProfilerView.onUseShader();
            gl.useProgram(shader);
            currentShader = shader;
        }
    }

    static ensureBatchIsBound(batch) {
        if (currentBatch !== batch) {
            uiProfilerView.onMeshUpload();
            gl.bufferData(gl.ARRAY_BUFFER, batch.vertices, gl.STATIC_DRAW);
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, batch.indices, gl.STATIC_DRAW);
            currentBatch = batch;
        }
    }

    static ensureTextureIsBound(texture) {
        if (currentTexture !== texture) {
            uiProfilerView.onTextureUpload();
            gl.activeTexture(gl.TEXTURE1);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.imageData);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
            currentTexture = texture;
        }
    }

    static forceTextureRebind(texture) {
        if (currentTexture === texture) {
            currentTexture = null;
        }
    }
}

RenderingContext.start();
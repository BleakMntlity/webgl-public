export class UiProfilerView {
    constructor() {
        this.container = document.createElement('div');
        this.frameTime = document.createTextNode('');
        this.fpsField = document.createTextNode('');
        
        this.renderers = document.createTextNode('');
        
        this.gpuMeshUploads = 0;
        this.gpuMeshUploadsField = document.createTextNode('');

        this.gpuTextureUploads = 0;
        this.gpuTextureUploadsField = document.createTextNode('');

        this.useShaderCalls = 0;
        this.useShaderCallsField = document.createTextNode('');

        this.drawCalls = 0;
        this.drawCallsField = document.createTextNode('')
        
        this.framesinLastSecond = 0;
        
        const now = Date.now();
        this.lastFpsUpdate = now;
        this.frameStart = now;

        const canvas = document.querySelector('#webglCanvas');

        this.container.appendChild(this.frameTime);
        this.container.appendChild(document.createTextNode(' '));
        this.container.appendChild(this.fpsField);
        this.container.appendChild(document.createTextNode(' '));
        this.container.appendChild(this.renderers);
        this.container.appendChild(document.createTextNode(' '));
        this.container.appendChild(this.drawCallsField);
        this.container.appendChild(document.createTextNode(' '));
        this.container.appendChild(this.gpuMeshUploadsField);
        this.container.appendChild(document.createTextNode(' '));
        this.container.appendChild(this.gpuTextureUploadsField);
        this.container.appendChild(document.createTextNode(' '));
        this.container.appendChild(this.useShaderCallsField);
        canvas.parentNode.insertBefore(this.container, canvas);
    }

    onTextureUpload() {
        ++this.gpuTextureUploads;
    }

    onMeshUpload() {
        ++this.gpuMeshUploads;
    }

    onUseShader() {
        ++this.useShaderCalls;
    }

    onDrawCalled() {
        ++this.drawCalls;
    }

    startFrame() {
        this.drawCalls = 0;
        this.gpuMeshUploads = 0;
        this.gpuTextureUploads = 0;
        this.useShaderCalls = 0;
        this.frameStart = Date.now();
    }

    endFrame(renderers) {
        const now = Date.now();
        const elapsedTime = now - this.frameStart;
        this.frameTime.textContent = 'Frame Time: ' + elapsedTime.toString() + 'ms';
        this.renderers.textContent = 'Drawed renderers: ' + renderers.toString();
        ++this.framesinLastSecond;

        if (now - this.lastFpsUpdate >= 1000.0) {
            this.lastFpsUpdate = now;
            this.fpsField.textContent = 'Locked FPS: ' + this.framesinLastSecond;
            this.framesinLastSecond = 0;
            this.lastFpsUpdate = now;
        }

        this.drawCallsField.textContent = 'Draw calls: ' + this.drawCalls.toString();
        this.gpuMeshUploadsField.textContent = 'Mesh uploads to gpu: ' + this.gpuMeshUploads.toString();
        this.gpuTextureUploadsField.textContent = 'Texture uploads to gpu: ' + this.gpuTextureUploads.toString();
        this.useShaderCallsField.textContent = 'Use shader calls: ' + this.useShaderCalls.toString();
    }
}

import { vertexShaderSrc, fragmentShaderSrc, shadowVertexShaderSrc, shadowFragmentShaderSrc, 
    backgroundVertexShaderSrc, backgroundFragmentShaderSrc } from './Shaders.js';

export class ShaderLoader {
    static loadStandardShader(gl) {
        return ShaderLoader.loadShader(vertexShaderSrc, fragmentShaderSrc, gl);
    }

    static loadShadowShader(gl) {
        return ShaderLoader.loadShader(shadowVertexShaderSrc, shadowFragmentShaderSrc, gl);
    }

    static loadBackgroundShader(gl) {
        return ShaderLoader.loadShader(backgroundVertexShaderSrc, backgroundFragmentShaderSrc, gl);
    }

    static loadShader(vertexSrc, fragmentSrc, gl) {
        const vertexShader = gl.createShader(gl.VERTEX_SHADER);
        const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

        gl.shaderSource(vertexShader, vertexSrc);
        gl.shaderSource(fragmentShader, fragmentSrc);

        gl.compileShader(vertexShader);
        let error = gl.getShaderInfoLog(vertexShader);
        if (error.length > 0) {
            console.error('vertex shader compile error: ' + error);
            gl.deleteShader(vertexShader)
            return;
        }

        gl.compileShader(fragmentShader);
        error = gl.getShaderInfoLog(fragmentShader);
        if (error.length > 0) {
            console.error('fragment shader compile error: ' + error);
            gl.deleteShader(fragmentShader)
            return;
        }

        const gpuProgram = gl.createProgram();
        gl.attachShader(gpuProgram, vertexShader);
        gl.attachShader(gpuProgram, fragmentShader);

        gl.linkProgram(gpuProgram);
        gl.validateProgram(gpuProgram);

        error = gl.getProgramInfoLog(gpuProgram);

        if (error.length > 0) {
            console.error('gpu program linking error: ' + error);
            gl.deleteProgram(gpuProgram);
            return;
        }

        gl.detachShader(gpuProgram, vertexShader);
        gl.deleteShader(vertexShader);

        gl.detachShader(gpuProgram, fragmentShader);
        gl.deleteShader(fragmentShader);

        return gpuProgram;
    }
}
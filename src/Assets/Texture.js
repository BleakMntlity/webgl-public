import { RenderingContext } from '../RenderingContext.js';

export class Texture {
    static createWithColor(width, height, r, g, b, a) {
        const texels = new Uint8ClampedArray(width * height * 4);

        for (let i = 0; i < texels.length; i += 4) {
            texels[i] = r;
            texels[i + 1] = g;
            texels[i + 2] = b;
            texels[i + 3] = a;
        }

        return new Texture(width, height, new ImageData(texels, width, height));
    }

    static loadFromUrl(width, height, url) {
        const image = new Image(width, height);
        const texture = Texture.createWithColor(width, height, 255, 255, 255, 255);
        image.onload = () => {
            texture.imageData = image;
            RenderingContext.forceTextureRebind(texture);
        };

        image.src = url;
        return texture;
    }

    constructor(width, height, imageData) {
        this.width = width;
        this.height = height;
        this.imageData = imageData;
    }
}
import { Mesh } from './Mesh.js';
import { quad, pyramid } from './MeshData.js';

export class MeshLoader {
    static loadQuad() {
        return new Mesh(quad.vertexData, quad.indices, quad.drawMode, quad.perVertexSize, quad.attributeOffsets);
    }

    static loadPyramid() {
        return new Mesh(pyramid.vertexData, pyramid.indices, 
            pyramid.drawMode, pyramid.perVertexSize, pyramid.attributeOffsets);
    }
}
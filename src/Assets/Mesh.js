export class Mesh {
    constructor(vertexData, indices, drawMode, perVertexSize, attributeOffsets) {
        this.vertexData = vertexData;
        this.indices = indices;
        this.drawMode = drawMode;
        this.perVertexSize = perVertexSize;
        this.attributeOffsets = attributeOffsets;
    }
}

import { vec3 } from '../glMatrix/gl-matrix.js';

const quadPos1 = [-1, -1, 0];
const quadPos2 = [-1, 1, 0];
const quadPos3 = [1, 1, 0];
const quadPos4 = [1, -1, 0];
const quadNormal = [0, 0, -1];
const quadVertex1 = quadPos1.concat(quadNormal).concat([1, 0]);
const quadVertex2 = quadPos2.concat(quadNormal).concat([1, 1]);
const quadVertex3 = quadPos3.concat(quadNormal).concat([0, 1]);
const quadVertex4 = quadPos4.concat(quadNormal).concat([0, 0]);
const quadVertices = quadVertex1.concat(quadVertex2).concat(quadVertex3).concat(quadVertex4);

const glTriangles = 4;

export const quad = {
    vertexData: quadVertices,
    indices: [0, 2, 1, 0, 3, 2],
    drawMode: glTriangles,
    perVertexSize: 32,
    attributeOffsets: {
        vertex: 0,
        normal: 12,
        uvs: 24
    },
};

const ppos1 = [-0.5, -0.5, -0.5];
const ppos2 = [0.5, -0.5, -0.5];
const ppos3 = [0.0, 0.5, 0.0];
const pnormalFrontSideVec3 = vec3.cross(vec3.create(), vec3.subtract(vec3.create(), ppos3, ppos1), vec3.subtract(vec3.create(), ppos2, ppos1), );
const pnormalFrontSide = [pnormalFrontSideVec3[0], pnormalFrontSideVec3[1], pnormalFrontSideVec3[2]];

const ppos4 = [-0.5, -0.5, -0.5];
const ppos5 = [-0.5, -0.5, 0.5];
const ppos6 = [0.5, -0.5, -0.5];
const ppos7 = [0.5, -0.5, 0.5];
const ppos8 = ppos5;
const ppos9 = ppos6;
const pnormalDownSide = [0, -1, 0];

const ppos10 = [-0.5, -0.5, -0.5];
const ppos11 = [-0.5, -0.5, 0.5];
const ppos12 = [0.0, 0.5, 0.0];
const pnormalLeftSideVec3 = vec3.cross(vec3.create(), vec3.subtract(vec3.create(), ppos11, ppos10), vec3.subtract(vec3.create(), ppos12, ppos10));
const pnormalLeftSide = [pnormalLeftSideVec3[0], pnormalLeftSideVec3[1], pnormalLeftSideVec3[2]];

const ppos13 = [0.5, -0.5, -0.5]
const ppos14 = [0.5, -0.5, 0.5];
const ppos15 = [0.0, 0.5, 0.0];
const pnormalRightSideVec3 = vec3.cross(vec3.create(), vec3.subtract(vec3.create(), ppos15, ppos13), vec3.subtract(vec3.create(), ppos14, ppos13));
const pnormalRightSide = [pnormalRightSideVec3[0], pnormalRightSideVec3[1], pnormalRightSideVec3[2]];

const ppos16 = [-0.5, -0.5, 0.5];
const ppos17 = [0.5, -0.5, 0.5];
const ppos18 = [0.0, 0.5, 0.0];
const pnormalBackSideVec3 = vec3.cross(vec3.create(), vec3.subtract(vec3.create(), ppos17, ppos16), vec3.subtract(vec3.create(), ppos18, ppos16));
const pnormalBackside = [pnormalBackSideVec3[0], pnormalBackSideVec3[1], pnormalBackSideVec3[2]];

const puvs = [0, 0];

const pverexData = ppos1.concat(pnormalFrontSide).concat([0, 0])
    .concat(ppos2).concat(pnormalFrontSide).concat([1, 0])
    .concat(ppos3).concat(pnormalFrontSide).concat([0.5, 1])
    .concat(ppos4).concat(pnormalDownSide).concat([0, 0])
    .concat(ppos5).concat(pnormalDownSide).concat([0, 1])
    .concat(ppos6).concat(pnormalDownSide).concat([1, 0])
    .concat(ppos7).concat(pnormalDownSide).concat([1, 1])
    .concat(ppos8).concat(pnormalDownSide).concat([0, 1])
    .concat(ppos9).concat(pnormalDownSide).concat([1, 0])
    .concat(ppos10).concat(pnormalLeftSide).concat([0, 0])
    .concat(ppos11).concat(pnormalLeftSide).concat([1, 0])
    .concat(ppos12).concat(pnormalLeftSide).concat([0.5, 1])
    .concat(ppos13).concat(pnormalRightSide).concat([0, 0])
    .concat(ppos14).concat(pnormalRightSide).concat([1, 0])
    .concat(ppos15).concat(pnormalRightSide).concat([0.5, 1])
    .concat(ppos16).concat(pnormalBackside).concat([0, 0])
    .concat(ppos17).concat(pnormalBackside).concat([1, 0])
    .concat(ppos18).concat(pnormalBackside).concat([0.5, 1]);

export const pyramid = {
    vertexData: pverexData,
    indices: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17],
    drawMode: glTriangles,
    perVertexSize: 32,
    attributeOffsets: {
        vertex: 0,
        normal: 12,
        uvs: 24
    }
}

const pfrontSideVertexData = ppos1.concat(pnormalFrontSide).concat(puvs)
    .concat(ppos2).concat(pnormalFrontSide).concat(puvs)
    .concat(ppos3).concat(pnormalFrontSide).concat(puvs);

export const pyramidFrontSide = {
    vertexData: pfrontSideVertexData,
    indices: [0, 1, 2],
    drawMode: glTriangles,
    perVertexSize: 32,
    attributeOffsets: {
        vertex: 0,
        normal: 12,
        uvs: 24
    }
}
export const vertexShaderSrc =
    'attribute vec3 vertex;\
attribute vec3 normal;\
attribute vec2 texcoord;\
\
varying lowp vec2 shadowMapCoords;\
varying highp float zInLightSpace;\
varying mediump vec2 uvCoords;\
varying lowp float vColor;\
\
uniform vec3 lightDirection;\
uniform mat4 mvpMatrix;\
uniform mat4 mlpMatrix;\
uniform mat4 worldToObject;\
\
void main()\
{\
    vec3 worldNormal = normalize(normal * mat3(worldToObject));\
    uvCoords = vec2(texcoord[0], texcoord[1]);\
    vColor = clamp(dot(worldNormal, lightDirection), 0.0, 1.0);\
    vec4 lightSpacePos = mlpMatrix * vec4(vertex, 1.0);\
    \
    shadowMapCoords = lightSpacePos.xy / lightSpacePos.w;\
    zInLightSpace = lightSpacePos.z / lightSpacePos.w;\
    gl_Position = mvpMatrix * vec4(vertex, 1.0);\
}';

export const fragmentShaderSrc =
    'uniform sampler2D texture;\
    uniform sampler2D shadowMap;\
\
varying lowp vec2 shadowMapCoords;\
varying highp float zInLightSpace;\
varying mediump vec2 uvCoords;\
varying lowp float vColor;\
\
precision mediump float;\
\
void main()\
{\
    vec4 texelColor = texture2D(texture, uvCoords);\
    vec4 shadow = texture2D(shadowMap, vec2(shadowMapCoords.x, shadowMapCoords.y));\
    float sampledZ = shadow.x + (shadow.y / 256.0) + (shadow.z / (256.0 * 256.0));\
    if (sampledZ >= zInLightSpace - 0.001)\
    {\
        gl_FragColor = vec4(min(vColor + 0.1, 1.0) * texelColor.rgb, texelColor.a);\
    }\
    else \
    {\
        gl_FragColor = vec4(0.1 * texelColor.rgb, texelColor.a);\
    }\
}';

export const backgroundVertexShaderSrc =
'attribute vec3 vertex;\
attribute vec3 normal;\
attribute vec2 texcoord;\
\
varying mediump vec2 uvCoords;\
\
void main()\
{\
    uvCoords = vec2(1.0 - texcoord.x, texcoord.y);\
    gl_Position = vec4(vertex, 1.0);\
}\
'

export const backgroundFragmentShaderSrc =
'uniform sampler2D shadowMap;\
\
varying mediump vec2 uvCoords;\
\
precision mediump float;\
\
void main()\
{\
    vec4 shadow = texture2D(shadowMap, vec2(uvCoords.x, uvCoords.y));\
    float sampledZ = shadow.x + (shadow.y / 256.0) + (shadow.z / (256.0 * 256.0));\
    if (sampledZ < 0.999)\
    {\
        gl_FragColor = vec4(0.1, 0.1, 0.1, 1.0);\
    }\
    else \
    {\
        gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);\
    }\
}';

export const shadowVertexShaderSrc =
    'attribute vec4 vertex;\
\
uniform mat4 mlpMatrix;\
\
void main()\
{\
    gl_Position = mlpMatrix * vertex;\
}';

export const shadowFragmentShaderSrc =
    'precision highp float;\
void main()\
{\
    gl_FragColor = vec4(gl_FragCoord.z, fract(gl_FragCoord.z * 256.0), fract(gl_FragCoord.z * 256.0 * 256.0), 1);\
}'